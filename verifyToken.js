const jwt = require('jsonwebtoken');
let config;
try { config = require('./.config'); }
catch(e) { /* No config in production */ }
const CLIENT_SECRET = process.env.CLIENT_SECRET || config.CLIENT_SECRET;

module.exports = (req, res, next) => {
    const token = req.headers['x-access-token'];
    if (!token) return res.status(403).send({ isAuth: false, message: 'No token provided' });
    jwt.verify(token, CLIENT_SECRET, (err, decoded) => {
        if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token' });
        req.userId = decoded.id;
        next();
    });
}