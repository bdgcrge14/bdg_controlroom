const fetch = require('node-fetch');
const request = require('request');
const chalk = require('chalk');
const { Pool } = require("pg");
const { getPreciseDistance } = require('geolib');
const _ = require('lodash');
const googleMapsClient = require('@google/maps').createClient({ key: 'AIzaSyBnWA-26UIDHgDBptlt--9mdb7SydYB5Vc' });

module.exports = (pgConfig, environment) => {
    let mod = {};

    const escape = (string) => {
        return (string) ? string.replace(/'/g, "''") : null
    };

    mod.parseResponse = (groupKey, response) => {
        let parsedResponse = {};
        const uniqueIdentifiers = [...new Set(response.map(c => c[groupKey]))];
        uniqueIdentifiers.forEach(ui => parsedResponse[ui] = response.filter(r => { return r[groupKey] === ui }));
        return parsedResponse
    };

    mod.getTransponderLimit = (req, res) => {
        const pool = new Pool(pgConfig);
        pool.query(`SELECT value FROM production.config WHERE key = 'TRANSPONDER_ERROR_DELAY_LIMIT'`)
        .then(result => {
            pool.end();
            if (result.rows.length === 1) return res.status(200).send(result.rows[0].value);
            return res.status(500).send('Error getting Transponder Error Delay Limit');
        })
        .catch(err => {
            pool.end();
        })
    };

    mod.getPortData = (req, res) => {
        const pool = new Pool(pgConfig);
        pool.query(`
            SELECT 
                p.id, 
                p.port_id, 
                p.port_name, 
                pr.port_location, 
                p.related_anchorage_id, 
                p.related_anchorage_name, 
                p.vessels_in_port, 
                p.expected_arrivals, 
                p.arrivals_last_24hrs, 
                p.departures_last_24hrs, 
                p.lat, 
                p.long, 
                p.current_offset, 
                p.created_at, 
                p.updated_at 
            FROM production.ports p
            JOIN production.port_ref pr ON pr.port_id = p.port_id
            WHERE p.updated_at > (select date_trunc('day', current_date - interval '1 hour'))`)
        .then(result => {
            pool.end();
            res.status(200).send(mod.parseResponse('port_id', result.rows));
        })
        .catch(err => {
            pool.end();
            res.status(500).send(err.stack);
        });
    };
    
    mod.getShipData = async (req, res, shipIDs) => {
        if (!shipIDs) return res.status(500).send(false);

        const shipsToOmit = [280923, 115152, 461962, 416564, 5614700, 158628, 730190, 299710, 116075, 320338, 5503, 5350368, 327246, 463156, 408252, 370244, 372344, 4012055, 4478581, 992074, 263477, 842];
        const chunkSize = 2;
        const chunkedArray = [..._.chunk(shipIDs.filter(id => { return !shipsToOmit.includes(id) }), chunkSize)];
        let chunkLength = 0;
        chunkedArray.forEach((c, idx) => { chunkLength = idx + 1 });
        try {
            let response = [];
            chunkedArray.forEach(async (chunk, idx) => {
                const pool = new Pool(pgConfig);
                // const client = await ship_pool.connect();
                pool.query(`
                    SELECT DISTINCT
                        s.id, 
                        s.ship_id, 
                        s.ship_name,
                        sr.ship_type, 
                        s.country, 
                        s.destination, 
                        pr0.port_id AS "uk_port_id",
                        pr0.port_name AS "uk_port_name",
                        pr0.lat AS "uk_port_lat",
                        pr0.long AS "uk_port_long",
                        pr0.port_location AS "uk_port_location",
                        pr1.port_id AS "eu_port_id",
                        pr1.port_name AS "eu_port_name",
                        pr1.lat AS "eu_port_lat",
                        pr1.long AS "eu_port_long",
                        pr1.port_location AS "eu_port_location",
                        r.journey_length,
                        r.journey_overwrite,
                        r.offset_length,
                        r.offset_overwrite,
                        s.current_port_id, 
                        s.current_port_name, 
                        s.next_port_id, 
                        s.next_port_name,
                        s.lat, 
                        s.long, 
                        s.last_position, 
                        s.distance_to_go, 
                        s.speed, 
                        s.created_at, 
                        s.updated_at
                    FROM production.ships s
                    INNER JOIN production.ship_ref sr ON sr.ship_id = s.ship_id
                    INNER JOIN production.port_ref pr0 ON pr0.id = sr.uk_port_id
                    INNER JOIN production.port_ref pr1 ON pr1.id = sr.eu_port_id
                    INNER JOIN production.route r ON r.uk_port = sr.uk_port_id AND r.eu_port = sr.eu_port_id
                    WHERE s.ship_id IN (${chunk.join(', ')})
                    AND s.updated_at > (select date_trunc('day', current_date - interval '24 hour'))
                    AND sr.enabled = true
                    ORDER BY s.ship_name, s.updated_at DESC`)
                .then(result => {
                    pool.end();
                    // ship_pool.query(`DROP TABLE IF EXISTS temp_ships_${idx}`);
    
    
                    let ships = {};
                    result.rows.forEach(s => {
                        if (ships[s.ship_id]) ships[s.ship_id].push(s);
                        else ships[s.ship_id] = [s];
                    });
    
                    const shipArray = Object.keys(ships)
                    .map(i => { return ships[i].sort((a, b) => { return (new Date(b.updated_at) - new Date(a.updated_at)) })[0]; })
                    .map(i => {
                        // get location of UK port, and EU port
                        const ukport = { port_id: i.uk_port_id, port_name: i.uk_port_name, lat: i.uk_port_lat, long: i.uk_port_long, port_location: i.uk_port_location }
                        const euport = { port_id: i.eu_port_id, port_name: i.eu_port_name, lat: i.eu_port_lat, long: i.eu_port_long, port_location: i.eu_port_location }
                        // getPreciseDistance from most recent ship location and ukport, and compare to the previous ship location and ukport
                        
                        const currentShipDistanceToUKPort = getPreciseDistance({ latitude: i.lat, longitude: i.long }, { latitude: ukport.lat, longitude: ukport.long });
                        const currentShipDistanceToEUPort = getPreciseDistance({ latitude: i.lat, longitude: i.long }, { latitude: euport.lat, longitude: euport.long });
                        
                        const directionScrapeBase = ships[i.ship_id].find((s, idx) => { return (ships[i.ship_id][idx+1]) ? s.lat != ships[i.ship_id][idx+1].lat || s.long != ships[i.ship_id][idx+1].long : false }) || ships[i.ship_id][0];
                        const shipDistanceToUKPort = getPreciseDistance({ latitude: i.uk_port_lat, longitude: i.uk_port_long }, { latitude: directionScrapeBase.lat, longitude: directionScrapeBase.long });
                        const prevShipDistanceToUKPort = getPreciseDistance({ lat: i.uk_port_lat, lon: i.uk_port_long }, { lat: ships[i.ship_id][ships[i.ship_id].indexOf(directionScrapeBase)+1].lat, lon: ships[i.ship_id][ships[i.ship_id].indexOf(directionScrapeBase)+1].long });
                        
                        const totalJourneyDistance = getPreciseDistance({ latitude: ukport.lat, longitude: ukport.long }, { latitude: euport.lat, longitude: euport.long });
    
                        let destination;
                        if (i.speed > 0.5) {
                            if (shipDistanceToUKPort > prevShipDistanceToUKPort) {
                                destination = euport;
                            } else {
                                destination = ukport
                            }
                        } else {
                            if (currentShipDistanceToUKPort < 100) {
                                destination = ukport;
                            } else {
                                destination = euport;
                            }
                        }
    
                        let distanceToGo, distanceTravelled, isDocked, lastInTransitScrape, firstDestinationScrape, lastOriginScrape, status;
    
                        const shipLocation = { latitude: i.lat, longitude: i.long };
                        const shipDestination = { latitude: destination.lat, longitude: destination.long };
                        const shipRemainingDistancePercent = (getPreciseDistance(shipLocation, shipDestination) / getPreciseDistance({ latitude: ukport.lat, longitude: ukport.long }, { latitude: euport.lat, longitude: euport.long })) * 100;
    
                        if (destination.port_location === 'UK') {
                            distanceToGo = (currentShipDistanceToUKPort / totalJourneyDistance) * 100;
                            distanceToGo = (distanceToGo < 12 && i.speed < 0.5) ? 0 : distanceToGo;
                            isDocked = distanceToGo === 0;
                        } else {
                            distanceToGo = (currentShipDistanceToEUPort / totalJourneyDistance) * 100;
                            distanceToGo = (distanceToGo < 12 && i.speed < 0.5) ? 0 : distanceToGo
                            distanceTravelled = 100 - distanceToGo;
                            isDocked = distanceTravelled === 100;
                        }
                        
                        if (!isDocked) {
                            // Most recent record of ship not in port
                            lastInTransitScrape = ships[i.ship_id].find(sh => 
                                (!sh.current_port_name || sh.current_port_name === 'null') && // Does not have a current port name
                                (ships[i.ship_id].indexOf(sh) > 0 && // Is not the last entry in the DB from scrape
                                    (new Date(ships[i.ship_id][ships[i.ship_id].indexOf(sh)-1].updated_at).getDate() === new Date(sh.updated_at).getDate()) && // Date of the previous entry and this entry are on the same day
                                    (ships[i.ship_id][ships[i.ship_id].indexOf(sh) - 1].current_port_name && ships[i.ship_id][ships[i.ship_id].indexOf(sh) - 1].current_port_name != 'null') // Previous entry does have a current port name
                                )
                            );
                            if (!lastInTransitScrape) return;
                            
                            firstDestinationScrape = ships[i.ship_id][ships[i.ship_id].indexOf(lastInTransitScrape) - 1]; // Most recent first instance of vessel in port after in transit
                            if (!firstDestinationScrape) return;
                            lastOriginScrape = ships[i.ship_id].find(sh => (sh.id < lastInTransitScrape.id) && (sh.current_port_name != 'null' && sh.current_port_name)); // Last instance of vessel in port prior to most recent first instance of vessel in port
                            if (!lastOriginScrape) return;
    
                            const averageTotalJourneyMinutes = Math.floor((new Date(firstDestinationScrape.updated_at).getTime() - new Date(lastOriginScrape.updated_at).getTime())/1000/60); // Minutes between leaving port & arriving at port
                            
                            const averageOnePercentTimeIncrement = averageTotalJourneyMinutes / 100; // Total journey is 100% therefore averageTotalJourneyMinutes/100 = N mins is 1%
                            const remainingTimeBasedOnAverage = shipRemainingDistancePercent * averageOnePercentTimeIncrement; // Calculate the remaining distance as time estimate from average
                            
                            const lastCurrentPortScrape = ships[i.ship_id].find(sh => {
                                return (ships[i.ship_id].indexOf(sh) > 0 && ((!ships[i.ship_id][ships[i.ship_id].indexOf(sh) - 1].current_port_name) || (ships[i.ship_id][ships[i.ship_id].indexOf(sh) - 1].current_port_name === 'null'))) && (sh.current_port_name && sh.current_port_name != 'null')
                            }); // Timestamp that the ship last left port

                            if (!lastCurrentPortScrape) return;
                            const timeTakenToCurrentPosition = Math.floor((new Date(ships[i.ship_id][0].updated_at).getTime() - new Date(lastCurrentPortScrape.updated_at))/1000/60); // Current time taken to current position
    
                            const elapsedShipTimeBasedOnAverage = Math.floor(averageTotalJourneyMinutes - remainingTimeBasedOnAverage); // Calculated ship time based on average calculations
                            
                            const diff = (i.journey_overwrite) ? averageTotalJourneyMinutes - i.journey_length : 0;
    
                            
                            const delayOffset = (i.offset_overwrite) ? i.offset_length : 15; //15min buffer (TODO: add in as user-updatable value)
    
                            if ((diff + elapsedShipTimeBasedOnAverage - delayOffset) > timeTakenToCurrentPosition) { // if the elapsed time minus the offset is ahead of the actual elapsed time, the ship is late
                                status = {
                                    name: 'late',
                                    value: (diff + elapsedShipTimeBasedOnAverage - delayOffset) - timeTakenToCurrentPosition
                                };
                            } else {
                                status = {
                                    name: 'early',
                                    value: (diff + elapsedShipTimeBasedOnAverage - delayOffset) - timeTakenToCurrentPosition
                                };
                            }
    
                        }
                        
                        if (isDocked) {
                            status = {
                                name: 'docked',
                                value: 0
                            };
                        }
    
                        // For each port
                        // Group by port name
                        // Query portStore.ports using portname and pass into props
                        return Object.assign(i,
                        {
                            destination, 
                            ukport, 
                            euport, 
                            totalDistance: totalJourneyDistance, 
                            distanceToUK: currentShipDistanceToUKPort, 
                            distanceToEU: currentShipDistanceToEUPort,
                            distanceTravelled,
                            status
                        });
                    });
    
                    shipArray.forEach(s => {
                        response.push(s)
                    });
    
                    chunkLength--;
    
                    if (chunkLength === 0) {
                        res.status(200).send(response);
                    }
                    return
                })
                .catch(err => {
                    if (!pool.ended) pool.end();
                    const pool2 = new Pool(pgConfig);
                    const query = `INSERT INTO ${environment}.error_log (
                        data,
                        created_at,
                        updated_at
                    ) VALUES ($1, $2, $3) RETURNING *`
                    pool2
                    .query(query, [err, new Date(), new Date()])
                    .then(result => { 
                        res.status(500).send(result);
                        pool2.end();
                    })
                    .catch(err => { 
                        if (!pool2.ended) pool2.end();
                        res.status(500).send(err);
                    });
                });
                
            });
        }
        catch(error) {
            const pool2 = new Pool(pgConfig);
            const query = `INSERT INTO ${environment}.error_log (
                data,
                created_at,
                updated_at
            ) VALUES ($1, $2, $3) RETURNING *`
            pool2
            .query(query, [error, new Date(), new Date()])
            .then(result => { 
                res.status(500).send(result);
                pool2.end();
            })
            .catch(err => { 
                res.status(500).send(err);
                pool2.end();
            });
        }

        return
    };

    mod.getTrafficData = async (req, res) => {
        const googleApiToggle = await checkIfGoogleDirectionsEnabled();
        if (!googleApiToggle || googleApiToggle === "false") return res.status(200).send([]);
        const pool = new Pool(pgConfig);
        pool.query(`SELECT * FROM production.road_traffic`)
        .then(result => {
            pool.end();
            const a = _.groupBy(result.rows.filter(r => { return (new Date - new Date(r.updated_at)) < 1200000 }), r => { return r.port_name });
            const response = Object.keys(a).map(key => {
                const groupedPortData = a[key];
                return ({
                    id: groupedPortData[0].id,
                    port_name: groupedPortData[0].port_name,
                    duration_sec: groupedPortData.map(d => { return d.duration_sec }).reduce((a, b) => { return a + b }, 0),
                    duration_in_traffic_sec: groupedPortData.map(d => { return d.duration_in_traffic_sec }).reduce((a, b) => { return a + b }, 0),
                    updated_at: groupedPortData[0].updated_at
                })
            });
            res.status(200).send(response);
        })
        .catch(err => {
            if (!pool.ended) pool.end();
            res.status(500).send(err);
        })
    };

    mod.scrapePortData = async (portIDs) => {
        try {
            let scrapePromises = [];
            portIDs.forEach(id => {
                scrapePromises.push(
                    fetch(`https://www.marinetraffic.com/en/reports?asset_type=ports&columns=flag,portname,unlocode,photo,vessels_in_port,vessels_departures,vessels_arrivals,vessels_expected_arrivals,local_time,anchorage,geographical_area_one,geographical_area_two,coverage&quicksearch_port_id=${id}`, 
                    {
                        "credentials": "omit",
                        "headers": {
                            "accept": "application/json, text/plain, */*",
                            "accept-language": "en-US,en;q=0.9,en-GB;q=0.8",
                            "sec-fetch-mode": "cors",
                            "sec-fetch-site": "same-origin",
                            "vessel-image": "004a6d0fceb54efcbe0ada4a0a0cd28e7f15",
                            "x-requested-with": "XMLHttpRequest"
                        },
                        "method": "GET",
                        "mode": "cors"
                    }).then(res => {
                        if (res.status === 200) return res.json();
                    })
                );
            })
            Promise.all(scrapePromises).then(res => {
                const valueRows = res.map(r => {
                    const { PORT_ID, PORT_NAME, COUNTRY, RELATED_ANCH_ID, RELATED_ANCH_NAME, SHIPCOUNT, EXPECTEDCOUNT, ARRIVECOUNT, DEPARTCOUNT, CENTERY, CENTERX, CURRENT_OFFSET } = r.data[0];
                    return `(${PORT_ID}, '${escape(PORT_NAME)}', '${COUNTRY}', ${RELATED_ANCH_ID}, '${escape(RELATED_ANCH_NAME)}', ${SHIPCOUNT}, ${EXPECTEDCOUNT}, ${ARRIVECOUNT}, ${DEPARTCOUNT}, ${CENTERY}, ${CENTERX}, ${CURRENT_OFFSET}, current_timestamp, current_timestamp)`;
                });
                const columnList = ['port_id', 'port_name', 'country', 'related_anchorage_id', 'related_anchorage_name', 'vessels_in_port', 'expected_arrivals', 'arrivals_last_24hrs', 'departures_last_24hrs', 'lat', '"long"', 'current_offset', 'created_at', 'updated_at'];
                const query = `INSERT INTO ${environment}.ports (${columnList.join(', ')}) VALUES ${valueRows.join(', ')}`;
                
                const pool = new Pool(pgConfig);
                pool.query(query)
                .then(result => {
                    pool.end();
                    res.forEach(r => {
                        console.log(chalk.black.bgGreen(` Successfully added port: ${r.data[0].PORT_NAME} `));
                    });
                })
                .catch(err => {
                    pool.end();
                    console.log(chalk.black.bgRed(` <${new Date().toLocaleTimeString()}> ${err.message} `));
                });
            });
        }
        catch (err) {
            console.log(chalk.black.bgRed(` <${new Date().toLocaleTimeString()}> ${err.message} `));
        }
    };
    
    mod.scrapeShipData = async (shipIDs) => {
        try {
            let scrapePromises = [];
            shipIDs.forEach(id => {
                scrapePromises.push(
                    fetch(`https://www.marinetraffic.com/en/reports?asset_type=vessels&columns=flag,shipname,photo,recognized_next_port,reported_eta,reported_destination,current_port,imo,ship_type,show_on_live_map,time_of_latest_position,lat_of_latest_position,lon_of_latest_position&quicksearch_shipid=${id}`,
                    {
                        "credentials": "omit",
                        "headers": {
                            "accept": "application/json, text/plain, */*",
                            "accept-language": "en-US,en;q=0.9,en-GB;q=0.8",
                            "sec-fetch-mode": "cors",
                            "sec-fetch-site": "same-origin",
                            "vessel-image": "004a6d0fceb54efcbe0ada4a0a0cd28e7f15",
                            "x-requested-with": "XMLHttpRequest"
                        },
                        "method": "GET",
                        "mode": "cors"
                    }).then(res => {
                        if (res.status === 200) return res.json();
                    })
                );
            });
            Promise.all(scrapePromises).then(res => {
                const valueRows = res.map(r => {
                    const { SHIP_ID, SHIPNAME, COUNTRY, DESTINATION, PORT_ID, CURRENT_PORT, NEXT_PORT_ID, NEXT_PORT_NAME, LAT, LON, LAST_POS, DISTANCE_TO_GO, SPEED } = r.data[0];
                    return `(${SHIP_ID}, '${escape(SHIPNAME)}', '${COUNTRY}', '${escape(DESTINATION)}', ${PORT_ID}, '${escape(CURRENT_PORT)}', ${NEXT_PORT_ID}, '${escape(NEXT_PORT_NAME)}', ${LAT}, ${LON}, ${LAST_POS}, ${DISTANCE_TO_GO}, ${SPEED}, current_timestamp, current_timestamp)`;
                });
                const columnList = ['ship_id', 'ship_name', 'country', 'destination', 'current_port_id', 'current_port_name', 'next_port_id', 'next_port_name', 'lat', '"long"', 'last_position', 'distance_to_go', 'speed', 'created_at', 'updated_at'];
                const query = `INSERT INTO ${environment}.ships (${columnList.join(', ')}) VALUES ${valueRows.join(', ')}`;

                const pool = new Pool(pgConfig);
                pool.query(query)
                .then(result => {
                    pool.end();
                    res.forEach(r => {
                        console.log(chalk.black.bgGreen(` Successfully added ship: ${r.data[0].SHIPNAME} `));
                    });
                })
                .catch(err => {
                    pool.end();
                    console.log(chalk.black.bgRed(` <${new Date().toLocaleTimeString()}> ${err.message} `));
                });
            });
        }
        catch (err) {
            console.log(chalk.black.bgRed(` <${new Date().toLocaleTimeString()}> ${err.message} `));
        }
    };

    mod.scrapeTrafficData = () => {
        request(`https://storage.googleapis.com/bdg/journeyRefList.txt`, (a,b,c) => {
            let z = c.split("// ,")[0].replace(/\n/g, " ");
            let journeys = JSON.parse(`[${z}]`);
            const journeysByInterval = _.groupBy(journeys, j => { return j.interval });
            Object.keys(journeysByInterval).forEach(i => {
                const interval = parseInt(i);
                trafficDataScrape(journeysByInterval[i]);
                setInterval(() => trafficDataScrape(journeysByInterval[i]), interval);
            });
        });
    }

    const trafficDataScrape = async (journeys) => {
        try {
            const googleApiToggle = await checkIfGoogleDirectionsEnabled();
            if (!googleApiToggle || googleApiToggle === "false") return;
            
            let journeyPromises = [];
            journeys.forEach((j, idx) => {
                const { origin, destination, interval } = j;
                const waypoints = j.waypoints.map(w => { return `via:${w}` });
                const params = { 
                    origin: origin, 
                    destination: destination, 
                    waypoints: waypoints,
                    departure_time: new Date().getTime()
                };
                journeyPromises.push(
                    new Promise((resolve, reject) => {
                        googleMapsClient.directions(params, (err, response) => {
                            if (!err) resolve({
                                id: j.id,
                                port_name: j.port,
                                duration: response.json.routes[0].legs[0].duration.value,
                                duration_in_traffic: response.json.routes[0].legs[0].duration_in_traffic.value
                            });
                        });
                    })
                );
            });
            Promise.all(journeyPromises).then(result => {
                const pool = new Pool(pgConfig);
                result.forEach(async (r, idx) => {
                    const client = await pool.connect();
                    const query = `
                        UPDATE production.road_traffic
                        SET port_name = '${r.port_name}', duration_sec = ${r.duration}, duration_in_traffic_sec = ${r.duration_in_traffic}, updated_at = current_timestamp
                        WHERE id = ${r.id}`;
                    client.query(query)
                    .then(r => {
                        client.release();
                        if (idx === result.length-1) {
                            pool.end();
                        }
                    })
                    .catch(err => {
                        console.log(err.message);
                        client.release()
                        if (idx === result.length-1 && !pool.ended) {
                            pool.end();
                        }
                    })
                });
            });
        }
        catch (e) {
            console.log(e.message);
        }
    };

    const checkIfGoogleDirectionsEnabled = async () => {
        const googleApiPool = new Pool(pgConfig);
        return googleApiPool
        .query(`SELECT DISTINCT value FROM production.config WHERE key = 'GOOGLE_DIRECTIONS_API_ENABLED'`)
        .then(res => {
            console.log("Google Traffic Enabled: " + res.rows[0].value);
            if (res.rows.length > 0) return res.rows[0].value;
            else return null;
        });
    };

    return mod;
};