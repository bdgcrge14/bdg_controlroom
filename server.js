let pgConfig = {};
let config;
try { config = require('./.config'); }
catch(e) { /* No config in production */ }

const environment = 'production'; // test | production
let PORT;
if (environment === 'production') {
    pgConfig.connectionString = `${process.env.DATABASE_URL}?ssl=true`;
    PORT = process.env.PORT;
} else {
    pgConfig = {
        user: 'uenkvij9bkqrmn',
        host: 'ec2-63-32-194-80.eu-west-1.compute.amazonaws.com',
        database: 'd655nrgo234c8b',
        password: 'pcf58755e450ff89886716ce51d6cb36e83653aa28a351ffe6670b273103def5a',
        port: '5432',
        ssl: true
    };
    PORT = process.env.PORT || 8080
}
const CLIENT_SECRET = process.env.CLIENT_SECRET || config.CLIENT_SECRET;

const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
const axios = require('axios');
const https = require("https");
const zlib = require("zlib");
const bcrypt = require("bcryptjs");
const { Pool } = require("pg");

const VerifyToken = require('./verifyToken');

const VesselTrackingService = require('./api/vesselTrackingService')(pgConfig, environment);
let portIDs, shipIDs;

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


//Serving the files in the dist folder
const DIST_DIR = path.join(__dirname, "dist");
app.use(express.static(DIST_DIR));


// Put all API endpoints under '/api'
app.post('/api/register', (req, res) => {
    const { user, email, password } = req.body;
    bcrypt.hash(password, 10).then((hash) => {
        // Store hash in your password DB.
        const pool = new Pool(pgConfig);
        const query = `
            INSERT INTO ${environment}."user"(
                username, 
                email, 
                hash, 
                created_at, 
                updated_at
            ) VALUES ($1, $2, $3, $4, $5) RETURNING *`;
        pool.query(query, [user, email, hash, new Date(), new Date()])
        .then(users => {
            pool.end();
            res.status(200).json(users[0]);
        })
        .catch(err => {
            pool.end()
            res.status(500).send(err);
        });
    });
});

app.post('/api/login', (req, res) => {
    const { username, password } = req.body;
    const pool = new Pool(pgConfig);
    const query = `
        SELECT 
            id, 
            username, 
            email, 
            hash,
            type, 
            created_at, 
            updated_at
        FROM ${environment}."user"
        WHERE username = '${username}'`;
    pool.query(query, (err, result) => {
        pool.end();
        if (err) {
            res.status(500).send(err.stack);
        } else {
            if (result.rows.length < 1) {
                res.status(401).send(result);
            } else {
                const { id, hash, type } = result.rows[0];
                bcrypt.compare(password, hash).then((bcryptResult) => {
                    if (bcryptResult) {
                        const params = (type != 'admin') ? { expiresIn: 900 } : null;
                        const token = jwt.sign({ id: id }, CLIENT_SECRET, params);
                        res.status(200).json({isAuth: true, token});
                    } else {
                        res.status(500).json({isAuth: false});
                    }
                });
            }
        }
    });
});

app.get('/api/eurotunnel', (req, res) => {
    axios.get('https://www.eurotunnelfreight.com/Source/Handlers/LatestTravellerInfo.ashx').then(result => {
        if (result.status === 200) {
            res.status(200).json(result.data);
        }
    });
});

app.get('/api/trafficenglandalerts', (req, res) => {
    axios.get('http://www.trafficengland.com/api/events/getAlerts?start=0&step=100&order=Severity&is_current=1&events=CONGESTION,INCIDENT&unconfirmed=false&completed=false&includeUnconfirmedRoadworks=true&_=1565345135725').then(result => {
        if (result.status === 200) {
            const formattedAlerts = result.data.map(a => {
                const eventType = {
                    "INCIDENT": 'Incident',
                    "CONGESTION": 'Congestion'
                };
                const severity = {
                    'HIGHEST': 'Severe',
                    'HIGH': 'Severe',
                    'MEDIUM': 'Moderate',
                    'LOW': 'Minor',
                    'LOWEST': 'Minor'
                };
                return {
                    id: a.id,
                    road: a.roadName,
                    type: { 
                        imgUrl: `http://www.trafficengland.com${(a.teEventType === "CONGESTION") ? "/resources/images/traffic-report/current-congestions.png" : "/resources/images/traffic-report/current-incidents.png"}`,
                        text: eventType[a.teEventType]
                    },
                    severity: severity[a.severity],
                    description: a.formatDesc,
                    lanes: a.eventLanes
                }
            });
            res.status(200).json(formattedAlerts);
        }
    });
});

app.get('/api/longrangeweather', (req, res) => {
    const options = {
        "method": "GET",
        "hostname": "www.metoffice.gov.uk",
        "path": "/weather/long-range-forecast",
        "headers": {
            "User-Agent": "PostmanRuntime/7.16.3",
            "Accept": "*/*",
            "Cache-Control": "no-cache",
            "Postman-Token": "c19dc954-f637-4151-8ff7-516b32db4bee,708a4358-1f42-4abb-ad18-7c965c6fc2f0",
            "Host": "www.metoffice.gov.uk",
            "Accept-Encoding": "gzip, deflate",
            "Connection": "keep-alive",
            "cache-control": "no-cache"
        },
        "gzip": true
    };

    const request = https.request(options, (result) => {
        let chunks = [];

        result.on("data", (chunk) => {
            chunks.push(chunk);
        });

        result.on("end", () => {
            const buffer = Buffer.concat(chunks);
            zlib.gunzip(buffer, (err, decoded) => {
                res.status(200).send(decoded);
            });
        });
    });

    request.end();
});

app.get('/api/portdata', VerifyToken, (req, res) => VesselTrackingService.getPortData(req, res, portIDs));

app.get('/api/shipdata', VerifyToken, (req, res) => VesselTrackingService.getShipData(req, res, shipIDs));

app.post('/api/updateRouteTime', VerifyToken, (req, res) => {
    const { uk_port_id, eu_port_id, journey_overwrite, journey_length } = req.body;
    const pool = new Pool(pgConfig);
    pool.query(`
        UPDATE ${environment}.route r
        SET journey_length = $1, journey_overwrite = $2, updated_at = current_timestamp
        FROM ${environment}.port_ref pr0, ${environment}.port_ref pr1 
        WHERE pr0.port_id = $3 AND pr1.port_id = $4 AND  r.uk_port = pr0.id AND r.eu_port = pr1.id
        RETURNING *`, [journey_length, journey_overwrite, uk_port_id, eu_port_id])
    .then(result => {
        pool.end();
        res.status(200).send(result);
    })
    .catch(err => {
        pool.end();
        res.status(500).send(err.stack);
    })
});

app.post('/api/updateOffsetTime', VerifyToken, (req, res) => {
    const { uk_port_id, eu_port_id, offset_overwrite, offset_length } = req.body;
    const pool = new Pool(pgConfig);
    pool.query(`
        UPDATE ${environment}.route r
        SET offset_length = $1, offset_overwrite = $2, updated_at = current_timestamp
        FROM ${environment}.port_ref pr0, ${environment}.port_ref pr1 
        WHERE pr0.port_id = $3 AND pr1.port_id = $4 AND  r.uk_port = pr0.id AND r.eu_port = pr1.id
        RETURNING *`, [offset_length, offset_overwrite, uk_port_id, eu_port_id])
    .then(result => {
        pool.end();
        res.status(200).send(result);
    })
    .catch(err => {
        pool.end();
        res.status(500).send(err.stack);
    })
});

app.get('/api/getTransponderLimit', VerifyToken, VesselTrackingService.getTransponderLimit);

app.get('/api/porttraffic', VerifyToken, (req, res) => { VesselTrackingService.getTrafficData(req, res); });

//Send index.html when the user access the web
app.get("*", (req, res) => { res.sendFile(path.join(DIST_DIR, "index.html")) });

const getShipIDs = async () => {
    const pool = new Pool(pgConfig);
    try {
        return pool.query(`SELECT ship_id from production.ship_ref ORDER BY ship_id ASC`).then(result => {
            pool.end()
            return shipIDs = result.rows.map(s => { return s.ship_id});
        });
    }
    catch(e) {
        pool.end();
        return shipIDs = [];
    }
};

const getPortIDs = async () => {
    const pool = new Pool(pgConfig);
    try {
        return pool.query(`SELECT port_id from production.port_ref ORDER BY port_id ASC`).then(result => {
            pool.end();
            return portIDs = result.rows.map(r => { return r.port_id});
        })
    }
    catch(e) {
        pool.end();
        return portIDs = [];
    }
};

app.listen(PORT, async () => {
    console.log(`Server running on port ${PORT}`);
    await getPortIDs();
    await getShipIDs();
    if (environment === 'production') {
        await VesselTrackingService.scrapeTrafficData();
        await VesselTrackingService.scrapePortData(portIDs);
        await VesselTrackingService.scrapeShipData(shipIDs);

        setInterval(getPortIDs, 120000);
        setInterval(getShipIDs, 120000);

        setInterval(() => { https.get('https://controlroom104.herokuapp.com/') }, 300000);
        setInterval(() => VesselTrackingService.scrapePortData(portIDs), 3660000);
        setInterval(() => VesselTrackingService.scrapeShipData(shipIDs), 120000);
    }
});