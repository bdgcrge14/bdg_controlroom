import { action, observable } from 'mobx';
import axios from 'axios';

export default class TrafficStore {
    @observable trafficFeeds = [
        { name: "Dover", src: "https://embed.waze.com/iframe?zoom=13&lat=51.1189&lon=1.3238&pin=0"},
        { name: "Portsmouth", src: "https://embed.waze.com/iframe?zoom=12&lat=50.8147551&lon=-1.098498&pin=0"},
        { name: "Calais", src: "https://embed.waze.com/iframe?zoom=13&lat=50.9713&lon=1.8636&pin=0"},
        { name: "Holyhead", src: "https://embed.waze.com/iframe?zoom=13&lat=53.3161&lon=-4.6328&pin=0"},
        { name: "Humber", src: "https://embed.waze.com/iframe?zoom=11.5&lat=53.6848471&lon=-0.2801091&pin=0"},
        { name: "Dunkirk", src: "https://embed.waze.com/iframe?zoom=13&lat=51.0488&lon=2.3758&pin=0"}
    ];
}