import { action, observable } from "mobx";
// import { auth, filter } from '@planet/client';
import Axios from "axios";

export default class TrafficStore {
    @observable imageItems = [];

    @action
    fetchSatelliteData() {
        fetch("https://api.planet.com/data/v1/quick-search?_sort=acquired%20desc&_page_size=250", {
            "credentials": "include",
            "headers": {
                "accept": "application/json",
                "authorization": "api-key e79c86d8fd714785b92061cba171138a", 
                "content-type": "application/json", 
                "sec-fetch-mode": "cors"
            },
            "referrerPolicy": "no-referrer-when-downgrade",
            "body": "{\"filter\":{\"type\":\"AndFilter\",\"config\":[{\"type\":\"GeometryFilter\",\"field_name\":\"geometry\",\"config\":{\"type\":\"Polygon\",\"coordinates\":[[[1.8368,50.9624],[1.8767,50.9624],[1.8767,50.9752],[1.8368,50.9752],[1.8368,50.9624]]]}},{\"type\":\"OrFilter\",\"config\":[{\"type\":\"AndFilter\",\"config\":[{\"type\":\"StringInFilter\",\"field_name\":\"item_type\",\"config\":[\"PSScene4Band\"]},{\"type\":\"OrFilter\",\"config\":[{\"type\":\"RangeFilter\",\"field_name\":\"visible_percent\",\"config\":{\"gte\":75,\"lte\":100}},{\"type\":\"AndFilter\",\"config\":[{\"type\":\"NotFilter\",\"config\":{\"type\":\"RangeFilter\",\"field_name\":\"visible_percent\",\"config\":{\"gte\":0,\"lte\":100}}},{\"type\":\"RangeFilter\",\"field_name\":\"cloud_cover\",\"config\":{\"gte\":0,\"lte\":0.25}}]}]},{\"type\":\"RangeFilter\",\"field_name\":\"sun_elevation\",\"config\":{\"gte\":0,\"lte\":90}}]},{\"type\":\"AndFilter\",\"config\":[{\"type\":\"StringInFilter\",\"field_name\":\"item_type\",\"config\":[\"PSScene3Band\"]},{\"type\":\"OrFilter\",\"config\":[{\"type\":\"RangeFilter\",\"field_name\":\"visible_percent\",\"config\":{\"gte\":75,\"lte\":100}},{\"type\":\"AndFilter\",\"config\":[{\"type\":\"NotFilter\",\"config\":{\"type\":\"RangeFilter\",\"field_name\":\"visible_percent\",\"config\":{\"gte\":0,\"lte\":100}}},{\"type\":\"RangeFilter\",\"field_name\":\"cloud_cover\",\"config\":{\"gte\":0,\"lte\":0.25}}]}]},{\"type\":\"RangeFilter\",\"field_name\":\"sun_elevation\",\"config\":{\"gte\":0,\"lte\":90}}]},{\"type\":\"AndFilter\",\"config\":[{\"type\":\"StringInFilter\",\"field_name\":\"item_type\",\"config\":[\"Landsat8L1G\"]},{\"type\":\"OrFilter\",\"config\":[{\"type\":\"RangeFilter\",\"field_name\":\"visible_percent\",\"config\":{\"gte\":75,\"lte\":100}},{\"type\":\"AndFilter\",\"config\":[{\"type\":\"NotFilter\",\"config\":{\"type\":\"RangeFilter\",\"field_name\":\"visible_percent\",\"config\":{\"gte\":0,\"lte\":100}}},{\"type\":\"RangeFilter\",\"field_name\":\"cloud_cover\",\"config\":{\"gte\":0,\"lte\":0.25}}]}]},{\"type\":\"RangeFilter\",\"field_name\":\"sun_elevation\",\"config\":{\"gte\":0,\"lte\":90}}]}]},{\"type\":\"OrFilter\",\"config\":[{\"type\":\"DateRangeFilter\",\"field_name\":\"acquired\",\"config\":{\"gte\":\"2019-05-16T10:38:57.112Z\",\"lte\":\"2019-08-16T10:38:57.112Z\"}}]}]},\"item_types\":[\"PSScene4Band\",\"PSScene3Band\",\"Landsat8L1G\"]}",
            "method": "POST",
            "mode": "cors"
        }).then(res => {
            return res.json();
        }).then(result => {
            this.imageItems = result.features.filter(f => {
                return f.properties.visible_confidence_percent && f.properties.visible_confidence_percent > 60;
            });
        });
    }
}