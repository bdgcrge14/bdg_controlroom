import { action, observable } from 'mobx';
import axios from 'axios';

export default class AlertsTrafficWeatherStore {
    @observable alerts = {
        loading: true,
        items: []
    };
    @observable weatherForecastHTML;

    @action
    getAlerts() {
        axios.get('/api/trafficenglandalerts')
        .then(res => {
            if (res.status === 200) {
                this.alerts = {
                    items: res.data,
                    loading: false
                }
            }
        });
    }

    @action
    getWeather() {
        axios.get('/api/longrangeweather')
        .then(res => {
            if (res.status === 200) {
                this.weatherForecastHTML = res;
            }
        });
    }
}