import { action, observable, reaction } from 'mobx';
import axios from 'axios';

export default class AuthStore {
    @observable isAuthenticated = window.localStorage.getItem('bdgAuth');
    @observable loginResponse;
  
    constructor() {
        reaction(
            () => this.isAuthenticated,
            isAuthenticated => {
                if (isAuthenticated) {
                    window.localStorage.setItem("bdgAuth", isAuthenticated);
                } else {
                    window.localStorage.removeItem("bdgAuth");
                }
            }
        );
    }
  
    @action
    login(value) {
        axios.post(`/api/login`, value)
        .then(res => {
            if (res.status === 200 & res.data.isAuth) {
                this.loginResponse = null;
                window.localStorage.setItem('bdgAuth', true)
                window.localStorage.setItem('bdgAuthToken', res.data.token)
                this.isAuthenticated = true;
                window.location = "/";
            } else {
                this.loginResponse = "Something's gone wrong.\nPlease try again later";
                window.localStorage.removeItem('bdgAuth');
                window.localStorage.removeItem('bdgAuthToken');
            }
        })
        .catch(err => {
        if (err.response.status === 500) {
            this.loginResponse = "There has been an error accessing the database to verify your details.\nPlease try again later.";
            window.localStorage.removeItem('bdgAuth');
            window.localStorage.removeItem('bdgAuthToken');
        } else if (err.response.status === 401) {
            this.loginResponse = "Invalid login details.\nPlease note, credentials are case sensitive.";
            window.localStorage.removeItem('bdgAuth');
            window.localStorage.removeItem('bdgAuthToken');
        } else {
            this.loginResponse = "Something's gone wrong.\nPlease try again later";
            window.localStorage.removeItem('bdgAuth');
            window.localStorage.removeItem('bdgAuthToken');
        }
        });
    }
  
    @action
    logout() {
        window.localStorage.removeItem('bdgAuth');
        window.localStorage.removeItem('bdgAuthToken');
        window.location = "/login";
    }
}