import { action, observable } from 'mobx';
import axios from 'axios';

export default class EurotunnelStore {
    @observable terminalInfos = {
        loading: true,
        items: []
    };
    @observable travelInfos;
    @observable parisToLondonTrains = {
        loading: true,
        items: []
    };
    @observable londonToParisTrains = {
        loading: true,
        items: []
    };

    @action
    getData() {
        const fetch = () => axios.get('/api/eurotunnel').then(res => {
            if (res.status === 200) {
                this.terminalInfos = {
                    loading: false,
                    items: res.data.TerminalInfos
                };
                this.travelInfos = res.data.TravelInfos;
            }
        });
        fetch();
        setInterval(() => { fetch(); }, 10000);
    }

    @action
    getEurostarSchedule() {
        const getParisToLondon = () => fetch("https://api.prod.eurostar.com/departures/graphql", {
            "credentials": "omit",
            "headers": { 
                "accept": "application/json, text/plain, */*",
                "content-type": "application/json;charset=UTF-8", 
                "market": "uk-en", 
                "sec-fetch-mode": "cors",
                "x-apikey": "457a1da6fea24444ade6153029cd5919"
            },
            "referrer": "https://departures.eurostar.com/",
            "referrerPolicy": "strict-origin-when-cross-origin",
            "body": "{\"query\":\"\\n  {\\n    trains(from: 8727100, to: 7015400) {\\n      number\\n      carrier\\n      origin {\\n        ...juncture\\n      }\\n      destination {\\n        ...juncture\\n      }\\n      reason\\n    }\\n  }\\n\\n  fragment juncture on Juncture {\\n    id\\n    time\\n    date\\n    tomorrow\\n    station {\\n      name\\n    }\\n    status {\\n      code\\n      text\\n    }\\n  }\\n\"}",
            "method": "POST",
            "mode": "cors"}).then(res => {
            if (res.status === 200) {
                return res.json();
            }
        }).then(res => {
            this.parisToLondonTrains = {
                loading: false,
                items: res.data.trains
            };
        });
        const getLondonToParis = () => fetch("https://api.prod.eurostar.com/departures/graphql", {
            "credentials": "omit",
            "headers": {
                "accept":"application/json, text/plain, */*",
                "content-type": "application/json;charset=UTF-8",
                "market": "uk-en",
                "sec-fetch-mode": "cors",
                "x-apikey": "457a1da6fea24444ade6153029cd5919"
            },
            "referrer": "https://departures.eurostar.com/",
            "referrerPolicy": "strict-origin-when-cross-origin",
            "body": "{\"query\":\"\\n  {\\n    trains(from: 7015400, to: 8727100) {\\n      number\\n      carrier\\n      origin {\\n        ...juncture\\n      }\\n      destination {\\n        ...juncture\\n      }\\n      reason\\n    }\\n  }\\n\\n  fragment juncture on Juncture {\\n    id\\n    time\\n    date\\n    tomorrow\\n    station {\\n      name\\n    }\\n    status {\\n      code\\n      text\\n    }\\n  }\\n\"}",
            "method": "POST",
            "mode": "cors" }).then(res => {
                if (res.status ===200) {
                    return res.json();
                }
            }).then(res => {
                this.londonToParisTrains = {
                    loading: false,
                    items: res.data.trains
                };
            });
        getParisToLondon();
        getLondonToParis();
        setInterval(() => { 
            getParisToLondon();
            getLondonToParis();
        }, 3600000)
    }
}