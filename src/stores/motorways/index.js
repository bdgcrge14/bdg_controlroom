import { action, observable } from 'mobx';
import axios from 'axios';

export default class MotorwayStore {
    @observable motorwayFeeds = [
        { name: "Junction 11", src: "https://embed.waze.com/iframe?zoom=15&lat=51.095383&lon=1.049623&ct=livemap"},
        { name: "Junction 9", src: "https://embed.waze.com/iframe?zoom=15&lat=51.160615&lon=0.867920&ct=livemap"},
        { name: "Junction 8", src: "https://embed.waze.com/iframe?zoom=15&lat=51.263875&lon=0.613689&ct=livemap"},
        { name: "Junction 7", src: "https://embed.waze.com/iframe?zoom=15&lat=51.287983&lon=0.551934&ct=livemap"},
        { name: "Junction 5", src: "https://embed.waze.com/iframe?zoom=15&lat=51.296115&lon=0.484471&ct=livemap"},
        { name: "Junction 2", src: "https://embed.waze.com/iframe?zoom=15&lat=51.388347&lon=0.193892&ct=livemap"}
    ];
}