import { action, observable } from 'mobx';
import Axios from 'axios';

import AuthStore from '../auth';
const authStore = new AuthStore();

export default class PortStore {

    @observable portFeeds = [
        { id: 154, name: "Dover", src: "https://www.marinetraffic.com/en/ais/embed/zoom:14/centery:51.1189/centerx:1.3238/maptype:2/shownames:false/mmsi:0/shipid:0/fleet:/fleet_id:/vtypes:/showmenu:true/remember:false"},
        { id: null, name: "Short Straits", src: "https://www.marinetraffic.com/en/ais/embed/zoom:10/centery:51.0345/centerx:1.5134/maptype:1/shownames:false/mmsi:0/shipid:0/fleet:/fleet_id:/vtypes:/showmenu:true/remember:false"},
        { id: 155, name: "Calais", src: "https://www.marinetraffic.com/en/ais/embed/zoom:13/centery:50.9713/centerx:1.8636/maptype:2/shownames:false/mmsi:0/shipid:0/fleet:/fleet_id:/vtypes:/showmenu:true/remember:false"},
        { id: 431, name: "Holyhead", src: "https://www.marinetraffic.com/en/ais/embed/zoom:13/centery:53.3161/centerx:-4.6328/maptype:2/shownames:false/mmsi:0/shipid:0/fleet:/fleet_id:/vtypes:/showmenu:true/remember:false"},
        { id: 107, name: "Dublin", src: "https://www.marinetraffic.com/en/ais/embed/zoom:13/centery:53.3454/centerx:-6.2069/maptype:2/shownames:false/mmsi:0/shipid:0/fleet:/fleet_id:/vtypes:/showmenu:true/remember:false"},
        { id: 156, name: "Dunkirk", src: "https://www.marinetraffic.com/en/ais/embed/zoom:13/centery:51.0488/centerx:2.3758/maptype:2/shownames:false/mmsi:0/shipid:0/fleet:/fleet_id:/vtypes:/showmenu:true/remember:false"}
    ];
    @observable portTraffic;
    @observable transponderLimit = 0;

    headerConfig = {
        headers: {
            'Access-Control-Allow-Headers': 'x-access-token',
            'x-access-token': window.localStorage.getItem('bdgAuthToken') || null
        }
    };

    @action
    getPortData() {
        return Axios.get('/api/portdata', this.headerConfig)
        .then(res => {
            if (res.status === 500 && ['failed to authenticate token', 'no token provided'].includes(response.data.message.toLowerCase())) {
                authStore.logout();
            }
            if (res.status === 200) {
                let ports = {};
                Object.keys(res.data).forEach(key => {
                    const port_name = res.data[key][0].port_name.toLowerCase().replace(/\s/g,'');
                    ports[port_name] = res.data[key];
                });
                return {
                    loading: false,
                    ports
                }
            }
        })
        .catch(({response}) => {
            if ([403, 500].includes(response.status) && ['failed to authenticate token', 'no token provided'].includes(response.data.message.toLowerCase())) {
                authStore.logout();
            }
        });
    }

    @action
    getVesselData() {
        return Axios.get('/api/shipdata', this.headerConfig).then(res => {
            if (res.status === 500 && ['failed to authenticate token', 'no token provided'].includes(response.data.message.toLowerCase())) {
                authStore.logout();
            }
            if (res.status === 200) {
                return {
                    loading: false,
                    vessels: res.data
                };
            }
        })
        .catch(({response}) => {
            if ([403, 500].includes(response.status) && ['failed to authenticate token', 'no token provided'].includes(response.data.message.toLowerCase())) {
                authStore.logout();
            }
        });
    }

    @action
    getPortTrafficData() {
        Axios.get('/api/porttraffic', this.headerConfig)
        .then(res => {
            if (res.status === 500 && ['failed to authenticate token', 'no token provided'].includes(response.data.message.toLowerCase())) {
                authStore.logout();
            }
            if (res.status === 200) this.portTraffic = res.data;
        })
        .catch(({response}) => {
            if ([403, 500].includes(response.status) && ['failed to authenticate token', 'no token provided'].includes(response.data.message.toLowerCase())) {
                authStore.logout();
            }
        })
    }

    @action
    updateRouteTime(uk_port_id, eu_port_id, journey_overwrite, journey_length) {
        Axios
        .post('/api/updateRouteTime', { uk_port_id, eu_port_id, journey_overwrite, journey_length }, this.headerConfig)
        .then(res => {
            if (res.status === 500 && ['failed to authenticate token', 'no token provided'].includes(response.data.message.toLowerCase())) {
                authStore.logout();
            }
            console.log("update route time result:");
            console.log(res);
        })
        .catch(({response}) => {
            if ([403, 500].includes(response.status) && ['failed to authenticate token', 'no token provided'].includes(response.data.message.toLowerCase())) {
                authStore.logout();
            }
        });
    }

    @action
    updateOffsetTime(uk_port_id, eu_port_id, offset_overwrite, offset_length) {
        Axios
        .post('/api/updateOffsetTime', { uk_port_id, eu_port_id, offset_overwrite, offset_length}, this.headerConfig)
        .then(res => {
            if (res.status === 500 && ['failed to authenticate token', 'no token provided'].includes(response.data.message.toLowerCase())) {
                authStore.logout();
            }
            console.log("update offset time result:");
            console.log(res);
        })
        .catch(({response}) => {
            if ([403, 500].includes(response.status) && ['failed to authenticate token', 'no token provided'].includes(response.data.message.toLowerCase())) {
                authStore.logout();
            }
        });
    }

    @action
    getTransponderLimit() {
        return Axios
        .get('/api/gettransponderlimit', this.headerConfig)
        .then(res => {
            if (res.status === 500 && ['failed to authenticate token', 'no token provided'].includes(response.data.message.toLowerCase())) {
                authStore.logout();
            }
            this.transponderLimit = res.data;
        })
        .catch(({response}) => {
            if ([403, 500].includes(response.status) && ['failed to authenticate token', 'no token provided'].includes(response.data.message.toLowerCase())) {
                authStore.logout();
            }
        })
    }
}