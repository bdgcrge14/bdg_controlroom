import React, { Component } from 'react';

import "./eurostarListItem.styl"

export default class EurostarListItem extends Component {
    render() {
        const { item } = this.props;
        let itemClassName;
        if ([item.origin.status.code, item.destination.status.code].includes('route-alert')) {
            itemClassName = 'alert';
        }
        if ([item.origin.status.code, item.destination.status.code].includes('delayed-exact')) {
            itemClassName = 'delay';
        }
        /*
            Train Statuses

            departed - rgb(119, 201, 100)
            arrived - rgb(119, 201, 100)
            route-alert - rgb(237, 106, 90)
            delayed-exact - rgb(253, 155, 69)
            on-time - none
        */
        const statusIcon = {
            "departed": <span className="fa-stack fa-lg"><i className="fa fa-circle fa-stack-2x departed"></i><i className="fa fa-check fa-stack-1x fa-inverse"></i></span>,
            "arrived": <span className="fa-stack fa-lg"><i className="fa fa-circle fa-stack-2x arrived"></i><i className="fa fa-check fa-stack-1x fa-inverse"></i></span>,
            "route-alert": <span className="fa-stack fa-lg"><i className="fa fa-circle fa-stack-2x route-alert"></i><i className="fa fa-exclamation fa-stack-1x fa-inverse"></i></span>,
            "delayed-exact": <span className="fa-stack fa-lg"><i className="fa fa-circle fa-stack-2x delayed-exact"></i><i className="fa fa-exclamation fa-stack-1x fa-inverse"></i></span>,
            "on-time": null
        };
        
        const reason = (item.reason) ? new DOMParser().parseFromString(item.reason, 'text/html').firstChild.innerText : null;

        return (
            <div id="eurostar-list-item" className={itemClassName}>
                <div className="train-name"><span>{item.carrier}</span><span>{item.number}</span></div>
                <div className="lower-content">
                    <div className="departure-station">
                        <div className="time">{item.origin.time}</div>
                        <div className="station-name">{item.origin.station.name}</div>
                        <div className="status">
                            <span className="icon">{statusIcon[item.origin.status.code]}</span>
                            <span>{item.origin.status.text}</span>
                        </div>
                    </div>
                    <div className="divider"><i className="fa fa-long-arrow-right"></i></div>
                    <div className="arrival-station">
                        <div className="time">{item.destination.time}</div>
                        <div className="station-name">{item.destination.station.name}</div>
                        <div className="status">
                            <span className="icon">{statusIcon[item.destination.status.code]}</span>
                            <span>{item.destination.status.text}</span>
                        </div>
                    </div>
                </div>
                { ([item.origin.status.code, item.destination.status.code].includes('route-alert')) ? <div className="reason">{reason}</div> : null }
            </div>
        )
    }
}