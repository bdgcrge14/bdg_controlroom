import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import './login.styl';

@inject('authStore')
@observer
export default class LoginView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: ''
        }
    }
    render() {
        return (
            <div id="login-view">
                <div className="login-form">
                    <div className="username-input">
                        <div className="label">Username</div>
                        <input type="text" className="text" onChange={(e) => this.onChange('username', e.target.value)} value={this.state.username} />
                    </div>
                    <div className="password-input">
                        <div className="label">Password</div>
                        <input type="password" className="text" onChange={(e) => this.onChange('password', e.target.value)} value={this.state.password} />
                    </div>
                    <button className="submit-button" onClick={this.submit.bind(this)}>Login</button>
                    {(this.props.authStore.loginResponse) ? <div className="error-text">{this.props.authStore.loginResponse}</div> : null}
                </div>
            </div>
        )
    }

    onChange(key, value) {
        this.props.authStore.loginResponse = null;
        let obj = {};
        obj[key] = value;
        this.setState(obj);
    }

    submit() {
        this.props.authStore.loginResponse = null;
        this.props.authStore.login(this.state);
    }
}