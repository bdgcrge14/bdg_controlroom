import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { inject, observer } from 'mobx-react';

@inject('authStore')
@observer
export default class PrivateRoute extends Component {
    render() {
        const { authStore, ...restProps } = this.props;
        if (authStore.isAuthenticated) return <Route {...restProps} />;
        return <Redirect to="/login" />;
    }
}