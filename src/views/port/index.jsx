import React, { Component } from "react";
import { inject, observer } from "mobx-react";

import "./styles.styl";

@inject('portStore')
@observer
export default class PortView extends Component {
    componentDidMount() {
        this.props.portStore.getPortData();
        this.props.portStore.getVesselData();
    }

    render() {
        const mapItems = this.props.portStore.portFeeds.map(feed => {
            return (
                <div key={feed.name} className="map-item">
                    <div className="title">{feed.name}</div>
                    <div className="map-container">
                        <iframe id={`${feed.name.toLowerCase()}_iframe`} src={feed.src} frameBorder="0"></iframe>
                    </div>
                </div>
            );
        });
        return (
            <div id="port-info">
                {mapItems}
            </div>
        );
    }
}