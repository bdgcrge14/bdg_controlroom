import React, { Component } from 'react';

import "./styles.styl"

export default class RailView extends Component {

    render() {
        return (
            <div>
                <div id="extract"></div>
                <iframe is="x-frame-bypass" src="https://www.opentraintimes.com/maps/signalling/xtd4#T_ASHFKY" />
            </div>
        )
    }
}