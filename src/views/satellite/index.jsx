import React, { Component } from 'react';

import './styles.styl';
import { inject, observer } from 'mobx-react';

@inject('satelliteStore')
@observer
export default class SatelliteView extends Component {
    componentDidMount() {
        this.props.satelliteStore.fetchSatelliteData();
    }
    render() {
        const items = this.props.satelliteStore.imageItems.map(image => {
            console.log(JSON.parse(JSON.stringify(image)));
            return (
                <div key={`${image.properties.provider}_${image.properties.item_type}_${image.id}`}>
                    <span><img src={`${image._links.thumbnail}?api_key=e79c86d8fd714785b92061cba171138a`} /></span><span>{image.id}</span>
                    <br/>
                </div>
            )
        });
        return (
            <div>
                {items}
            </div>
        )
    }
}