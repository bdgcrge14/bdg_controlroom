import React, { Component } from "react";
import { inject } from "mobx-react";

import "./styles.styl";

@inject('trafficStore')
export default class TrafficView extends Component {
    render() {
        const mapItems = this.props.trafficStore.trafficFeeds.map(feed => {
            return (
                <div key={feed.name} className="map-item">
                    <div className="title">{feed.name}</div>
                    <div className="map-container">
                        <iframe id={`${feed.name.toLowerCase()}_iframe`} src={feed.src}></iframe>
                    </div>
                </div>
            );
        });
        return (
            <div id="traffic-info">
                {mapItems}
            </div>
        );
    }
}