import React, { Component } from 'react';
import { inject, observer } from "mobx-react";
import moment from 'moment';
const flagUK = 'https://storage.googleapis.com/bdg/flag_uk.png';
const flagEU = 'https://storage.googleapis.com/bdg/flag_eu.png';
const lorryLeft = "https://storage.googleapis.com/bdg/lorry_left.png";
const lorryRight = "https://storage.googleapis.com/bdg/lorry_right.png";
const waterPattern = "https://storage.googleapis.com/bdg/water_pattern.png"
const shipLeft = 'https://storage.googleapis.com/bdg/ship_left.png';
const unaccompaniedShipLeft = 'https://storage.googleapis.com/bdg/unaccompanied_ship_left.png';
const shipLeftRed = "https://storage.googleapis.com/bdg/ship_left_red.png";
const unaccompaniedShipLeftRed = 'https://storage.googleapis.com/bdg/unaccompanied_ship_left_red.png';
const shipLeftGreen = "https://storage.googleapis.com/bdg/ship_left_green.png";
const unaccompaniedShipLeftGreen = 'https://storage.googleapis.com/bdg/unaccompanied_ship_left_green.png';
const shipRight = 'https://storage.googleapis.com/bdg/ship_right.png';
const unaccompaniedShipRight = 'https://storage.googleapis.com/bdg/unaccompanied_ship_right.png';
const shipRightRed = "https://storage.googleapis.com/bdg/ship_right_red.png";
const unaccompaniedShipRightRed = 'https://storage.googleapis.com/bdg/unaccompanied_ship_right_red.png';
const shipRightGreen = "https://storage.googleapis.com/bdg/ship_right_green.png";
const unaccompaniedShipRightGreen = 'https://storage.googleapis.com/bdg/unaccompanied_ship_right_green.png';

import "./journeyItem.styl";


@inject('portStore')
@observer
export default class JourneyItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { id, ship_name, ship_type, speed, ukport, euport, destination, totalDistance, distanceToUK, distanceToEU, distanceTravelled, status,  } = this.props;
        let distanceToGo, shipImg, shipImgStyle, delayLabelStyle, dataError;

        const ukPortTrafficData = this.props.portStore.portTraffic.find(p => p.port_name === ukport.port_name);
        const euPortTrafficData = this.props.portStore.portTraffic.find(p => p.port_name === euport.port_name);
        let ukPortDelayClass = '', euPortDelayClass = '', ukPortDelayTimestamp = 'N/A', euPortDelayTimestamp = 'N/A';

        if (ukPortTrafficData) {
            const { duration_in_traffic_sec, duration_sec, updated_at } = ukPortTrafficData;
            ukPortDelayTimestamp = `@${moment(updated_at).format('HH:mm')}`;
            if (duration_in_traffic_sec <= duration_sec)
                ukPortDelayClass = 'green';
            else {
                const percentageIncrease = (duration_in_traffic_sec - duration_sec) / duration_sec * 100
                if (percentageIncrease >= 0 && percentageIncrease <= 10)
                    ukPortDelayClass = 'green';
                else if (percentageIncrease > 10 && percentageIncrease <= 25)
                    ukPortDelayClass = 'amber-1';
                else if (percentageIncrease > 25 && percentageIncrease <= 50)
                    ukPortDelayClass = 'amber-2';
                else if (percentageIncrease > 50 && percentageIncrease <= 100)
                    ukPortDelayClass = 'red-1';
                else if (percentageIncrease > 100 && percentageIncrease <= 200)
                    ukPortDelayClass = 'red-2';
                else if (percentageIncrease > 200)
                    ukPortDelayClass = 'black'
            }
        }

        if (euPortTrafficData) {
            const { duration_in_traffic_sec, duration_sec, updated_at } = euPortTrafficData;
            euPortDelayTimestamp = `@${moment(updated_at).format('HH:mm')}`;
            if (duration_in_traffic_sec <= duration_sec)
                euPortDelayClass = 'green';
            else {
                const percentageIncrease = (duration_in_traffic_sec - duration_sec) / duration_sec * 100
                if (percentageIncrease >= 0 && percentageIncrease <= 10)
                    euPortDelayClass = 'green';
                else if (percentageIncrease > 10 && percentageIncrease <= 25)
                    euPortDelayClass = 'amber-1';
                else if (percentageIncrease > 25 && percentageIncrease <= 50)
                    euPortDelayClass = 'amber-2';
                else if (percentageIncrease > 50 && percentageIncrease <= 100)
                    euPortDelayClass = 'red-1';
                else if (percentageIncrease > 100 && percentageIncrease <= 200)
                    euPortDelayClass = 'red-2';
                else if (percentageIncrease > 200)
                    euPortDelayClass = 'black'
            }
        }

        if (destination.port_location === 'UK') {
            distanceToGo = (distanceToUK / totalDistance) * 100;
            distanceToGo = (distanceToGo < 12 && speed < 0.5) ? 0 : distanceToGo;
            if (distanceToGo > 102 || status.value > this.props.portStore.transponderLimit) {
                distanceToGo = 100
                dataError = true;
                shipImg = (ship_type == 'ACCOMPANIED') ? shipLeft : unaccompaniedShipLeft;
                shipImgStyle = { "left": 0, "marginLeft": `${(distanceToGo < 87) ? 'calc('+distanceToGo+'%)' : 'calc('+distanceToGo+'% - (10vh * 2))' }` };
            } else {
                dataError = false;            
                switch(status.name) {
                    case 'late':
                        shipImg = (ship_type == 'ACCOMPANIED') ? shipLeftRed : unaccompaniedShipLeftRed;
                        break;
                    case 'early':
                        shipImg = (ship_type == 'ACCOMPANIED') ? shipLeftGreen : unaccompaniedShipLeftGreen;
                        break;
                    case 'docked': //TODO: port wait times
                        shipImg = (ship_type == 'ACCOMPANIED') ? shipLeftGreen : unaccompaniedShipLeftGreen;
                }
                shipImgStyle = { "left": 0, "marginLeft": `${(distanceToGo < 87) ? 'calc('+distanceToGo+'%)' : 'calc('+distanceToGo+'% - (10vh * 3))' }` };
            }
            // if distanceToGo > 80 then label is left
            let delayLabelMargin;
            if (distanceToGo > 80) {
                delayLabelMargin = (dataError) ? `calc(${distanceToGo}% - (10vh * 1))` : `calc(${distanceToGo}% - (10vh * 1.5))`;
            } else {
                delayLabelMargin = `calc(${distanceToGo}% + (10vh * 1.5))`;
            }
            delayLabelStyle = { "left": 0, "marginLeft": delayLabelMargin };
        } else {
            distanceToGo = (distanceToEU / totalDistance) * 100;
            distanceToGo = (distanceToGo < 12 && speed < 0.5) ? 0 : distanceToGo
            if (distanceToGo > 102 || status.value > this.props.portStore.transponderLimit) {
                distanceToGo = 100
                dataError = true;
                shipImg = (ship_type == 'ACCOMPANIED') ? shipRight : unaccompaniedShipRight;
                shipImgStyle = { "right": 0, "marginRight": `${(distanceToGo < 87) ? 'calc('+distanceToGo+'%)' : 'calc('+distanceToGo+'% - (10vh * 2))' }` };
            } else {
                dataError = false;
                switch(status.name) {
                    case 'late':
                        shipImg = (ship_type == 'ACCOMPANIED') ? shipRightRed : unaccompaniedShipRightRed;
                        break;
                    case 'early':
                        shipImg = (ship_type == 'ACCOMPANIED') ? shipRightGreen : unaccompaniedShipRightGreen;
                        break;
                    case 'docked': //TODO: port wait times
                        shipImg = (ship_type == 'ACCOMPANIED') ? shipRightGreen : unaccompaniedShipRightGreen;
                }
                shipImgStyle = { "right": 0, "marginRight": `${(distanceToGo < 87) ? 'calc('+distanceToGo+'%)' : 'calc('+distanceToGo+'% - (10vh * 3))' }` };
            }
            let delayLabelMargin;
            if (distanceToGo > 80) {
                delayLabelMargin = (dataError) ? `calc(${distanceToGo}% - (10vh * 1))` : `calc(${distanceToGo}% - (10vh * 1.5))`;
            } else {
                delayLabelMargin = `calc(${distanceToGo}% + (10vh * 1.5))`;
            }
            delayLabelStyle = { "right": 0, "marginRight": delayLabelMargin };
        }
        
        return (
            <div key={id} id="journey-item" className={(dataError) ? 'data-error' : null}>
                <div className="grey-pane">Transponder Data Issue</div>
                <img className="dock-flag flag_uk" src={flagUK}></img>
                <div className="origin">
                    <div className="port-container">
                        <div className={`road-container ${ukPortDelayClass}`}>
                            <div className={`port-delay-timestamp ${ukPortDelayClass}`}>{ukPortDelayTimestamp}</div>
                            <div className="road"></div>
                        </div>
                        <div className="dock-name">{ukport.port_name}</div>
                    </div>
                    {(status.name === 'docked' && destination.port_location === 'UK') ? <img className="lorry-image" src={lorryRight}></img> : null}
                </div>
                <div className="journey">
                    <img className="ship-image" style={shipImgStyle} src={shipImg}></img>
                    <div className="ship-name" style={shipImgStyle}>{ship_name}</div>
                    { (!dataError && (status.name != 'docked' && status.value > 0)) ? <div className="ship-delay-label" style={delayLabelStyle}>+{Math.floor(status.value)}</div> : null }
                    <div className="path" style={{'backgroundImage': `url(${waterPattern})`}}></div>
                </div>
                <div className="destination">
                    <div className="port-container">
                        <div className="dock-name">{(euport.port_name === 'OUISTREHAM') ? 'CAEN' : euport.port_name}</div>
                        <div className={`road-container ${euPortDelayClass}`}>
                            <div className={`port-delay-timestamp ${euPortDelayClass}`}>{euPortDelayTimestamp}</div>
                            <div className="road"></div>
                        </div>
                    </div>
                    {(status.name === 'docked' && destination.port_location === 'EU') ? <img className="lorry-image" src={lorryLeft}></img> : null}
                </div>
                <img className="dock-flag flag_eu" src={flagEU}></img>
            </div>
        )
    }
}