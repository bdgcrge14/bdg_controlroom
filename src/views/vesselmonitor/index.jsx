import React, { Component } from 'react';
import { inject, observer } from "mobx-react";
import Toggle from 'react-bootstrap-toggle';
import Spinner from '../components/spinner';
import JourneyItem from './journeyItem';
import _ from 'lodash';
const SeaviewLogoWhiteBg = 'https://storage.googleapis.com/bdg/seaview_logo_white_shadow.png'
const SeaviewLogoBlackBg = 'https://storage.googleapis.com/bdg/seaview_logo_black_shadow.png'

import "./styles.styl";

@inject('portStore')
@observer
export default class VesselMonitorView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sidebarOpen: false,
            portData: {
                loading: true,
                ports: []
            },
            vesselData: {
                loading: true,
                vessels: []
            },
            portFilter: [],
            overwrites: {},
            offsets: {}
        }
    }

    componentWillMount() {
        this.props.portStore.getTransponderLimit();
        this.props.portStore.getPortTrafficData();
        setInterval(() => { 
            this.props.portStore.getTransponderLimit();
            this.props.portStore.getPortTrafficData();
        }, 240000);
    }

    componentDidMount() {
        window.addEventListener('click', (e) => {
            if (this.state.sidebarOpen && !document.getElementById('sidebar').contains(e.target))
                this.setState({ sidebarOpen: false })
        });
        const getData = () => {
            this.props.portStore.getPortData().then(res => {
                this.setState({ portData: res });
            });
            this.props.portStore.getVesselData().then(async (res) => { 
                await this.setState({ vesselData: res });
                this.parseSidebarOverwrites() 
            });
        };
        getData();
        setInterval(() => { getData() }, 240000);
    }

    render() {
        const { portData: { ports }, vesselData } = this.state;
        
        let ships = vesselData.vessels;
        const journeyItems = ships
        .filter(s => {
            if (!s) return;
            return (this.state.portFilter.includes('late') || this.state.portFilter.length === 0) 
                ? true 
                : ports[s.uk_port_name.replace(/\s/g,'').toLowerCase()] && (this.state.portFilter.includes(ports[s.uk_port_name.replace(/\s/g,'').toLowerCase()][0].port_id) || this.state.portFilter.includes(ports[s.eu_port_name.replace(/\s/g,'').toLowerCase()][0].port_id))  
        })
        .sort((a, b) => { 
            if (a.uk_port_name > b.uk_port_name) {return 1; } 
            else if (b.uk_port_name > a.uk_port_name) { return -1; }
            
            if (a.eu_port_name > b.eu_port_name) { return 1; }
            else if (b.eu_port_name > a.eu_port_name) { return -1; }
            
            if (a.ship_name > b.ship_name) { return 1; } 
            else if (b.ship_name > a.ship_name) { return -1; }
            else { return 0; }
        })
        .map(i => {
            if (!i) return;
            if (this.state.portFilter.includes('late') && i.status.name != 'late') {
                return null;
            } else {
                return <JourneyItem key={i.id} {...i} destination={i.destination} ukport={i.ukport} euport={i.euport} totalDistance={i.totalDistance} distanceToUK={i.distanceToUK} distanceToEU={i.distanceToEU} distanceTravelled={i.distanceTravelled} status={i.status} />
            }
        });

        const shipUkPortList = [...new Set(ships.filter(i => { return i != null }).map(s => { return s.uk_port_name.toLowerCase().replace(/\s/g,'') }))];
        const shipEuPortList = [...new Set(ships.filter(i => { return i != null }).map(s => { return s.eu_port_name.toLowerCase().replace(/\s/g,'') }))];

        const sidebarPortFilteruk = Object.keys(ports).filter(p => { return shipUkPortList.includes(p) && (ports[p].length > 0) ? ports[p][0].port_location === 'UK' : false }).sort((a, b) => { return (a > b) ? 1 : ((b > a) ? -1 : 0) }).map(p => {
            if (ports[p] && ports[p].length > 0) {
                const port = ports[p][0];
                return (
                    <div key={port.port_id} className="port-select-item">
                        <input type="checkbox" className="checkbox" onChange={(e) => { this.togglePortFilter(e, port) }}></input>
                        <div className="port-name">{port.port_name}</div>
                    </div>
                )
            }
        });
        const sidebarPortFiltereu = Object.keys(ports).filter(p => { return shipEuPortList.includes(p) && (ports[p].length > 0) ? ports[p][0].port_location === 'EU' : false }).sort((a, b) => { return (a > b) ? 1 : ((b > a) ? -1 : 0) }).map(p => {
            if (ports[p] && ports[p].length > 0) {
                const port = ports[p][0];
                return (
                    <div key={port.port_id} className="port-select-item">
                        <input type="checkbox" className="checkbox" onChange={(e) => { this.togglePortFilter(e, port) }}></input>
                        <div className="port-name">{(port.port_name === 'OUISTREHAM') ? 'CAEN' : port.port_name}</div>
                    </div>
                )
            }
        });

        return (
            <>
                <div id="vessel-monitor-view">
                    { (!vesselData.loading) ?
                    <>
                        <div id="sidebar" className={(this.state.sidebarOpen) ? "sidebar open" : "sidebar"} onMouseEnter={() => { this.toggleSidebar(true) }}>
                            <div className="handle">
                                <i className="fa fa-caret-right"></i>
                            </div>
                            <div className="sidebar-inner-content">
                                <img className="seaview-logo" src={SeaviewLogoWhiteBg} />
                                <div className="support-text">For support, contact border.delivery.group.pmo@hmrc.gov.uk</div>
                                <div className="inner-content-options">
                                    <div className="filter-ports-container">
                                        <label>Filter ports</label>
                                        <div className="port-filter-list">
                                            <div className="filter-all-late">
                                                <div className="port-select-item">
                                                    <input type="checkbox" className="checkbox" onChange={(e) => { this.togglePortFilter(e, 'late') }}></input>
                                                    <div className="port-name">ALL LATE CROSSINGS</div>
                                                </div>
                                            </div>
                                            <div className="ports">
                                                <div className="uk-ports">{ sidebarPortFilteruk }</div>
                                                <div className="eu-ports">{ sidebarPortFiltereu }</div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                {/* <div className="controls">
                                    <button className="save-button">Save</button>
                                </div> */}
                            </div>
                        </div>
                        <div className="data-container">
                            <div className="table-container">
                                <div className="header">
                                    <div className="ship-name">Vessel Name</div>
                                    <div className="ship-destination">Route</div>
                                    <div className="ship-next-port-name">Stage</div>
                                    <div className="ship-id">Expected Time</div>
                                    <div className="ship-id">Actual Time</div>
                                </div>
                                <div className="rows">
                                    {/* { rowItems } */}
                                </div>
                            </div>
                        </div>
                        <div className="visual-container">
                            <div className="journey-container">
                                {journeyItems}
                            </div>
                        </div>
                    </>
                    : 
                    <div className="spin-container">
                        <img className="seaview-logo" src={SeaviewLogoBlackBg} />
                        <div>For support, contact border.delivery.group.pmo@hmrc.gov.uk</div>
                        <div>You can filter routes displayed by clicking on the left hand tab</div>
                        <div className="spinner">
                            <Spinner color={'rgba(0, 0, 0, 0.6)'} />
                        </div>
                    </div>
                    }
                </div>
                <div id="orientation-warning">
                    <div className="inner-content">
                        Please use device in landscape orientation to view the vessel tracking screen.
                    </div>
                </div>
            </>
        )
    }

    parseSidebarOverwrites() {
        const { vesselData: { vessels: ships }, portData: { ports } } = this.state;
        let ow = Object.assign({},this.state.overwrites);
        let os = Object.assign({}, this.state.offsets);
        _.uniqBy(ships.map(s => {
            if (!s) return;
            return {
                uk_port: s.uk_port_name,
                eu_port: s.eu_port_name,
                journey_length: s.journey_length,
                journey_overwrite: s.journey_overwrite,
                offset_length: s.offset_length,
                offset_overwrite: s.offset_overwrite
            }
        }).filter(r => { return r != null; }), a => [a.uk_port, a.eu_port].join() ).sort((a, b) => { return (a.uk_port > b.uk_port) ? 1 : ((b.uk_port > a.uk_port) ? -1 : 0) })
        .forEach(r => {
            if (!(ports[r.uk_port.replace(/\s/g,'').toLowerCase()] && ports[r.eu_port.replace(/\s/g,'').toLowerCase()]) || !(ports[r.uk_port.replace(/\s/g,'').toLowerCase()].length > 0 && ports[r.eu_port.replace(/\s/g,'').toLowerCase()].length > 0)) return;
            ow[`${ports[r.uk_port.replace(/\s/g,'').toLowerCase()][0].port_id}_${ports[r.eu_port.replace(/\s/g,'').toLowerCase()][0].port_id}`] = r.journey_length;
            os[`${ports[r.uk_port.replace(/\s/g,'').toLowerCase()][0].port_id}_${ports[r.eu_port.replace(/\s/g,'').toLowerCase()][0].port_id}`] = r.offset_length;
        });
        this.setState({ overwrites: ow, offsets: os });
    }

    toggleSidebar(val) {
        this.setState({ sidebarOpen: val })
    }

    togglePortFilter({target: { checked }}, port) {
        let arr = [...this.state.portFilter];
        if (port === 'late') {
            if (checked) {
                arr.push('late');
            } else {
                const idx = arr.indexOf('late');
                if (idx > -1) arr.splice(idx, 1);
            };
        } else {
            const { port_id } = port;
            if (checked) {
                arr.push(port_id);
            } else {
                const idx = arr.indexOf(port_id);
                if (idx > -1) arr.splice(idx, 1);
            }
        }
        this.setState({ portFilter: arr });
    }

    toggleRouteOverwrite(checked, uk_port_name, eu_port_name, ref ) {
        const { vesselData, portData } = this.state;
        const vesselClone = { ...vesselData };
        Object.keys(vesselClone.vessels).forEach(v => {
            if (vesselClone.vessels[v][0].uk_port === uk_port_name && vesselClone.vessels[v][0].eu_port === eu_port_name)
                vesselClone.vessels[v] = vesselClone.vessels[v].map(v => {
                    v.journey_overwrite = checked; 
                    v.journey_length = parseInt(this.refs[ref].value);
                    return v; 
                });
        });

        this.setState({ vesselData: vesselClone });
        this.props.portStore.updateRouteTime(portData.ports[uk_port_name.toLowerCase()][0].port_id, portData.ports[eu_port_name.toLowerCase()][0].port_id, checked, parseInt(this.refs[ref].value));
    }

    toggleOffsetOverwrite(checked, uk_port_name, eu_port_name, ref) {
        const { vesselData, portData } = this.state;
        const vesselClone = { ...vesselData };
        Object.keys(vesselClone.vessels).forEach(v => {
            if (vesselClone.vessels[v][0].uk_port === uk_port_name && vesselClone.vessels[v][0].eu_port === eu_port_name)
                vesselClone.vessels[v] = vesselClone.vessels[v].map(v => {
                    v.offset_overwrite = checked; 
                    v.offset_length = parseInt(this.refs[ref].value);
                    return v; 
                });
        });
        
        this.setState({ vesselData: vesselClone });
        this.props.portStore.updateRouteTime(portData.ports[uk_port_name.toLowerCase()][0].port_id, portData.ports[eu_port_name.toLowerCase()][0].port_id, checked, parseInt(this.refs[ref].value));
    }



    updateOverwrite(e, key) {
        let ow = Object.assign({}, this.state.overwrites);
        const val = (e.target.value) ? parseInt(e.target.value): 0;
        ow[key] = val;
        this.setState({ overwrites: ow });
    }
    updateOffset(e, key) {
        let os = Object.assign({}, this.state.offsets);
        const val = (e.target.value) ? parseInt(e.target.value) : 0;
        os[key] = val;
        this.setState({ offsets: os });        
    }
}