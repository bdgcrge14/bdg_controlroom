import React, { Component } from 'react';

import "./styles.styl";

export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            linkData: [
                {
                    url: '/eurotunnel',
                    text: 'Eurotunnel'
                },
                {
                    url: '/traffic',
                    text: 'Traffic'
                },
                {
                    url: '/alerts',
                    text: 'Alerts & Weather'
                },
                {
                    url: '/port',
                    text: 'Ports'
                },
                {
                    url: '/motorway',
                    text: 'Motorway'
                },
                {
                    url: '/vessel',
                    text: 'Vessel Tracking'
                }
            ]
        }
    }

    componentWillMount() {
        // let ld = [...this.state.linkData];
        // const rotation_increment = (90 / ld.length);
        // for(let i = 1; ld.length >= i; i++) {
        //     ld[i-1].style = { 
        //         transform: `translateY(-${i-1}rem) rotate(${-54+i*rotation_increment}deg)`,
        //     }
        // };
        // this.setState({ linkData: ld });
    }

    render() {
        return (
            <div className="links">
                {
                    this.state.linkData.map(ld => { 
                        return (
                            <div key={ld.url} className="hyperlink" style={ld.style} onClick={(e) => window.open(ld.url, '_blank', 'toolbar=0,location=0,menubar=0')}>{ld.text}</div>
                        )
                    })
                }
                <div className="hyperlink admin" onClick={(e) => window.open('https://bdgcra.herokuapp.com', '_blank', 'toolbar=0,location=0,menubar=0')}>Vessel Tracking Admin</div>
            </div>
        )
    }
}