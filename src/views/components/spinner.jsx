import React, { Component } from 'react';
import './spinner.styl';

export default class Spinner extends Component {
    render() {
        const fontColour = (this.props.color) ? this.props.color : 'black'
        return (
            <div id="spinner-component" style={{ 'color': fontColour }}>
                <i className="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                <div className="text">Loading...</div>
            </div>
        )
    }
}