import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import schedule from 'node-schedule';

import Spinner from '../components/spinner';
import './styles.styl'

@inject('alertsTrafficWeatherStore')
@observer
export default class AlertsTrafficWeatherView extends Component {
    componentDidMount() {
        this.props.alertsTrafficWeatherStore.getAlerts();
        this.props.alertsTrafficWeatherStore.getWeather();
        setInterval(() => { () => { 
            this.props.alertsTrafficWeatherStore.getAlerts();
            this.props.alertsTrafficWeatherStore.getWeather();
        }}, 10000);
    }

    componentDidUpdate() {
        const { weatherForecastHTML } = this.props.alertsTrafficWeatherStore;
        if (weatherForecastHTML.data) {
            let doc = document.getElementById('iframe').contentWindow.document;
            doc.open();
            doc.write(weatherForecastHTML.data.substring(weatherForecastHTML.data.indexOf('<main id="content">'), weatherForecastHTML.data.indexOf("</main>") + 7));
            doc.close();
        }
    }

    render() {
        const { alerts } = this.props.alertsTrafficWeatherStore;
        return(
            <div id="alerts-traffic-weather">
                <div id="alerts">
                    <div className="title">Traffic England Alerts</div>
                    {
                        (!alerts.loading) ?
                        <>
                            <div className="header-row">
                                <div id="row-heading" className="road-name">Road</div>
                                <div id="row-heading" className="alert-type">Type</div>
                                <div id="row-heading" className="alert-severity">Severity</div>
                                <div id="row-heading" className="alert-description">Description</div>
                            </div>
                            {
                                alerts.items.map(a => {
                                    return (
                                        <div key={a.id} className="alert-row">
                                            <div id="row-item" className="road-name">{a.road}</div>
                                            <div id="row-item" className="alert-type">
                                                <div className="icon" style={{"background":`url("${a.type.imgUrl}")`}}></div>
                                                <div className="text">{ a.type.text }</div>
                                            </div>
                                            <div id="row-item" className={`alert-severity ${a.severity.toLowerCase()}`}>{a.severity}</div>
                                            <div id="row-item" className="alert-description">
                                                { a.description.map(d => { return <span key={d}>{d}</span>}) }
                                                { 
                                                    (a.lanes.some(l => { return l.laneStatus === "CLOSED" })) 
                                                    ? <div className="lanes">Lanes Closed:&nbsp;{ a.lanes.map(l => {
                                                            return (l.laneStatus === "CLOSED") 
                                                            ? <div key={l.laneName} className={`lane-item ${(l.laneName === "Hard Shoulder") ? 'hs' : null}`}>
                                                                <img src="http://www.trafficengland.com/resources/images/common/normal-closed.png" alt={l.laneName}/>
                                                                <div>{l.laneName}</div>
                                                            </div>
                                                            : <div key={l.laneName} className={`lane-item ${(l.laneName === "Hard Shoulder") ? 'hs' : null}`}>
                                                                <img src="http://www.trafficengland.com/resources/images/common/normal-open.png" alt={l.laneName}/>
                                                                <div>{l.laneName}</div>
                                                            </div>
                                                        })}
                                                    </div> 
                                                    : null
                                                }
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </>
                        :
                        <Spinner color={'rgba(0, 0, 0, 0.6)'}/>
                    }
                </div>
                <div id="long-range-weather">
                    <div className="title">Long Range Forecast</div>
                    <div id="weather-content"></div>
                    <iframe id="iframe" onLoad={this.iframeLoaded.bind(this)}/>
                </div>
            </div>
        )
    }
    iframeLoaded() {
        const iframe = document.getElementById("iframe");
        const div = document.getElementById("weather-content");
        try {
            div.innerHTML = iframe.contentWindow.document.querySelector("#content > div.wrap-3-col > div.wrap-3-mid > div.content-container > article > div:nth-child(3) > div.col-md-8").innerHTML;
        }
        catch (e) {
            console.log(e);
        }
        schedule.scheduleJob('0 0 * * *', () => {
            iframe.contentWindow.location.reload();
        });
    }
}