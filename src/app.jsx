import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import PrivateRoute from './views/auth/private_route';
import LoginView from './views/auth/login';
import Dashboard from './views/dashboard';
import Eurotunnel from './views/eurotunnel';
import TrafficView from './views/traffic';
import MotorwayView from './views/motorways';
import PortView from './views/port';
import RailView from './views/rail';
import AlertsTrafficWeatherView from './views/alertstrafficweather';
import SatelliteView from './views/satellite';
import VesselMonitorView from './views/vesselmonitor';

class App extends Component {

    render() {
        return (
            <Switch>
                <Route path="/login" component={LoginView} />
                <PrivateRoute path="/" exact component={Dashboard} />
                <PrivateRoute path="/eurotunnel" component={Eurotunnel} />
                <PrivateRoute path="/traffic" component={TrafficView} />
                <PrivateRoute path="/motorway" component={MotorwayView} />
                <PrivateRoute path="/port" component={PortView} />
                {/* <PrivateRoute path="/rail" component={RailView} /> */}
                <PrivateRoute path="/alerts" component={AlertsTrafficWeatherView} />
                {/* <PrivateRoute path="/satellite" component={SatelliteView} /> */}
                <PrivateRoute path="/vessel" component={VesselMonitorView} />
            </Switch>
        );
    }
}

export default App;