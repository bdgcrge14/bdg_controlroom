require("babel-polyfill");
import React from "react";
import { render } from "react-dom";
import { Provider } from "mobx-react";
import { BrowserRouter } from "react-router-dom";
import DevTools from "mobx-react-devtools";

import AuthStore from './stores/auth';
import EurotunnelStore from './stores/eurotunnel';
import TrafficStore from './stores/traffic';
import MotorwayStore from './stores/motorways';
import PortStore from './stores/port';
import AlertsTrafficWeatherStore from './stores/alertstrafficweather';
import SatelliteStore from './stores/satellite'
const authStore = new AuthStore();
const eurotunnelStore = new EurotunnelStore();
const trafficStore = new TrafficStore();
const motorwayStore = new MotorwayStore();
const portStore = new PortStore();
const alertsTrafficWeatherStore = new AlertsTrafficWeatherStore();
const satelliteStore = new SatelliteStore();


import App from "./app";

Object.defineProperty(String.prototype, "toTitleCase", {
	value: function toTitleCase() {
		return this.split('_').map(word => word.replace(word[0], word[0].toUpperCase())).join(' ');
	},
	writable: true,
	configurable: true
});

render(
	<Provider 
		authStore={authStore}
		eurotunnelStore={eurotunnelStore}
		trafficStore={trafficStore} 
		motorwayStore={motorwayStore} 
		portStore={portStore}
		alertsTrafficWeatherStore={alertsTrafficWeatherStore}
		satelliteStore={satelliteStore}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>,
    document.getElementById("root")
);