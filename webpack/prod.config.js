var webpack = require('webpack');
var merge = require('webpack-merge');
var baseConfig = require("./base.config");
var { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = merge(baseConfig, {
  mode: 'production',
  plugins: [
    new CleanWebpackPlugin(),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ]
});
