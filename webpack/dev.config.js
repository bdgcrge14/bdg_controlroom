var path = require('path');
var webpack = require('webpack');
var merge = require('webpack-merge');
var baseConfig = require('./base.config');
var Dotenv = require('dotenv-webpack');

console.log(__dirname);
console.log(path.join(__dirname, "../config/dev.env"));

module.exports = merge(baseConfig, {
  mode: 'production',
  devtool: "eval-source-map",
  devServer: {
    historyApiFallback: true
  },
  plugins: [
    new Dotenv({
      path: path.join(__dirname, "../config/dev.env")
    }),
    new webpack.HotModuleReplacementPlugin()
  ]
});