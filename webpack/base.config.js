var path = require('path');
var webpack = require('webpack');

module.exports = {
  devtool: "eval",
  entry: ['babel-polyfill', './src/index'],
  // {
  //   app: "./src/index"
  // },
  output: {
    path: path.join(__dirname, "../dist"),
    publicPath: "/dist/",
    filename: "bundle.js"
  },
  resolve: {
    extensions: [".js", ".jsx", ".styl"]
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        exclude: "/node_modules/",
        use: ["babel-loader"],
        include: path.join(__dirname, "../src")
      },
      {
        test: /\.css$/,
        use: [{ loader: "css-loader" }]
      },
      {
        test: /\.styl$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader", options: { url: false } },
          { loader: "stylus-loader" }
        ]
      }
    ]
  },
  plugins: [
    new webpack.EnvironmentPlugin([
      'NODE_ENV',
    ])
  ]
};
